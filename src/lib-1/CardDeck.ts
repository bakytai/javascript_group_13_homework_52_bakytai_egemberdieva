import {isNumeric} from "rxjs/internal-compatibility";

const RANKS = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
const SUITS = ['diams', 'hearts', 'clubs', 'spades'];

export class Card {
  constructor(
    public rank: string,
    public suit: string) {}


  getScore()  {
    let number = 0;
    if (isNumeric(this.rank)) {
      number = parseInt(this.rank);
    }
    if (this.rank === 'J' || this.rank === 'Q' || this.rank === 'K') {
      number = 10;
    }
    if (this.rank === 'A') {
      number = 11;
    }
    return number
  }
}

export class CardDeck {
  cards: Card[] = [];

  constructor() {
    for (let i = 0; i < RANKS.length; i++) {
      for (let j = 0; j < SUITS.length; j++) {
        const rankValue = RANKS[i];
        const suitValue = SUITS[j];
        this.cards.push(new Card(rankValue, suitValue));
      }
    }
  }

  getCard() {
    const randomElement = this.cards[Math.floor(Math.random() * this.cards.length)];
    for (let i = 0; i < this.cards.length; i++) {
      if (randomElement === this.cards[i]) {
        this.cards.splice(i, 1);
      }
    }
    return randomElement;
  }

  getCards(howMany: number) {
    const cards: Card[] = [];

    for (let i = 0; i < howMany; i++) {
      cards.push(this.getCard());
    }
    return cards;
  }
}

