import {Component, Input} from '@angular/core';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() rank = '';
  @Input() suit = '';

  class = '';

  getClassname() {
    this.class = `card ${this.rank.toLowerCase()} ${this.suit}`;
    return this.class;
  }

  changeSuit(suit: string) {
    if (suit === 'diams') {
      return '♦';
    } else if (suit === 'hearts') {
      return  '♥';
    } else if (suit === 'clubs') {
      return  '♣';
    } else if (suit === 'spades') {
      return '♠';
    } else {
      return
    }
  }
}

