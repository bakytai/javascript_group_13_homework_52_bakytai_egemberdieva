import { Component } from '@angular/core';
import {Card, CardDeck} from "../lib-1/CardDeck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'blackJ';
  deck: CardDeck;
  twoCards: Card[] = [];

  constructor() {
    this.deck = new CardDeck();
    this.twoCards = this.deck.getCards(2);
  }

  getMoreCard() {
    const oneCard = this.deck.getCard();
    this.twoCards.push(oneCard);
  }

  getNewArray() {
    this.deck = new CardDeck();
    this.twoCards = this.deck.getCards(2);
  }

  getScore() {
    let counter = 0;
    this.twoCards.forEach(card => {
      counter += card.getScore();
    })
    return counter
  }
}
